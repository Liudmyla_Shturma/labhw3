const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const { getLoads, addLoad, getActiveLoad, changeActiveLoadState, getLoad, updateLoad, deleteLoad, postLoad, getLoadShippingInfo } = require('../controllers/loadController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.use(asyncWrapper(authMiddleware));

router
   .route('/')
   .get(asyncWrapper(getLoads))
   .post(asyncWrapper(addLoad));

router
   .route('/active')
   .get(asyncWrapper(getActiveLoad))

router
   .route('/active/state')
   .patch(asyncWrapper(changeActiveLoadState));

router
   .route('/:id')
   .get(asyncWrapper(getLoad))
   .put(asyncWrapper(updateLoad))
   .delete(asyncWrapper(deleteLoad));

router
   .route('/:id/post')
   .post(asyncWrapper(postLoad));

router
   .route('/:id/shipping_info')
   .get(asyncWrapper(getLoadShippingInfo));


module.exports = router;