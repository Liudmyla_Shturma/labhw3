const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const { getTrucks, addTruck, getTruck, updateTruck, deleteTruck, assignTruck } = require('../controllers/truckController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.use(asyncWrapper(authMiddleware));

router
   .route('/')
   .get(asyncWrapper(getTrucks))
   .post(asyncWrapper(addTruck));

router
   .route('/:id')
   .get(asyncWrapper(getTruck))
   .put(asyncWrapper(updateTruck))
   .delete(asyncWrapper(deleteTruck));

router
   .route('/:id/assign')
   .post(asyncWrapper(assignTruck));

module.exports = router;