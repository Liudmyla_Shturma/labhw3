const express = require('express');
const morgan = require('morgan');
const app = express();
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const { User } = require('./models/userModel');
const { Truck } = require('./models/truckModel');
const { Load } = require('./models/loadModel');


try {
        User.deleteMany();
        Truck.deleteMany();
        Load.deleteMany();
    } catch (err) {
      console.log(err);
    };


module.exports = app;