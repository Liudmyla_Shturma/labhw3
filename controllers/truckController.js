const { Truck } = require('../models/truckModel');
const APIFeatures = require('./../utils/apiFeatures');


// getTrucks, addTruck, getTruck, updateTruck, deleteTruck, assignTruck

exports.getTrucks = async (req, res) => {
  let filter = {};
  filter = { userId: req.user._id };

  const features = new APIFeatures(Truck.find(filter), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate();
  
    const Trucks = await features.query;
  
  res.status(200).json({
    status: 'success',
    results: Trucks.length,
    Trucks: Trucks
  });
  
};

exports.addTruck = async (req, res) => {

  const { text } = req.body;

  const Truck = new Truck({
  });

  await Truck.save();

  res.json({message: 'Sucess'});

};

exports.getTruck = async (req, res) => {
   const Truck = await Truck.findById(req.params.id);
   if (Truck) {
    res.json({message: 'Success', Truck: Truck});
   }
   
   res.status(400).json({message: 'Failure'});
};

exports.updateTruck = async (req, res) => {
  
  const Truck = await Truck.findById(req.params.id);

  Truck.text = req.body.text; 

  updatedTruck = await Truck.save();

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedTruck
    }
  });

};

exports.deleteTruck = async (req, res) => {
   const isdel = await Truck.findByIdAndDelete(req.params.id);

   if (!isdel) {
      res.status(400).json({message: 'Failure'});
   } else {
    res.json({message: 'Success'});
   }

};

exports.assignTruck = async (req, res) => {
  const Truck = await Truck.findById(req.params.id).select('+assigned_to');

  Truck.completed =  !Truck.completed;
  await Truck.save();

  res.json({message: 'Success'});
};