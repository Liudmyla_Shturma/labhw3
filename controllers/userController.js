const { User } = require('../models/userModel');

exports.getInfo = async (req, res) => {
    const user = await User.findById(req.user._id);
    if (!user) {
	      res.status(500).json({message: `User with id {%req.user._id} found`});
	      return;
     }

    res.status(200).json({user: user});
   
};


exports.deleteUser = async (req, res) => {
    const isdel = await User.findByIdAndDelete(req.user.id);

    if (!isdel) {
       res.status(400).json({message: 'Failure'});
    } else {
     res.json({message: 'Success'});
    }
 
};


exports.changePassword = async (req, res) => {
  const user = await User.findById(req.user._id).select('+password');
  
  if (user.password.localeCompare(req.body.oldPassword) == 0) {
    user.password = req.body.newPassword;
    await user.save();
    res.json({message: 'Success'});
  } else {
    res.status(400).json({message: 'Failure'});
  }

};