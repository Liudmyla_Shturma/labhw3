//const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    const { email,  password , role } = req.body;

    const user = new User({
        email,
        password,
        role
    });

    await user.save();

    res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {

    const { email, password } = req.body;

    const user = await User.findOne({email});

    if (!user) {
        res.status(400).json({message: `No user with username '${email}' found!`});
        return
    }

    if ( password.localeCompare(user.password) != 0 ) {
        res.status(400).json({message: `Wrong password!`});
        return
    }

    const jwt_token = jwt.sign({ username: user.email, _id: user._id }, JWT_SECRET);
    res.json({message: 'Success', jwt_token});

};