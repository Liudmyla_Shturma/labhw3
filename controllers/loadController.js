const { Load } = require('../models/loadModel');
const APIFeatures = require('./../utils/apiFeatures');

// getLoads, addLoad, getActiveLoad, changeActiveLoadState, getLoad, updateLoad, deleteLoad, postLoad, getLoadShippingInfo

exports.getLoads = async (req, res) => {
  let filter = {};
  filter = { userId: req.user._id };

  const features = new APIFeatures(Load.find(filter), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate();
  
    const Loads = await features.query;
  
  res.status(200).json({
    status: 'success',
    results: Loads.length,
    Loads: Loads
  });
  
};

exports.addLoad = async (req, res) => {

  const { text } = req.body;

  const Load = new Load({
  });

  await Load.save();

  res.json({message: 'Sucess'});

};

exports.getActiveLoad = async (req, res) => {
  const Load = await Load.findById(req.params.id);
  if (Load) {
   res.json({message: 'Success', Load: Load});
  }
  
  res.status(400).json({message: 'Failure'});
};

exports.changeActiveLoadState = async (req, res) => {

  const Load = await Load.findById(req.params.id);

  updatedLoad = await Load.save();

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedLoad
    }
  });


};

exports.getLoad = async (req, res) => {
   const Load = await Load.findById(req.params.id);
   if (Load) {
    res.json({message: 'Success', Load: Load});
   }
   
   res.status(400).json({message: 'Failure'});
};

exports.updateLoad = async (req, res) => {
  
  const Load = await Load.findById(req.params.id);

  Load.text = req.body.text; 

  updatedLoad = await Load.save();

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedLoad
    }
  });

};

exports.deleteLoad = async (req, res) => {
   const isdel = await Load.findByIdAndDelete(req.params.id);

   if (!isdel) {
      res.status(400).json({message: 'Failure'});
   } else {
    res.json({message: 'Success'});
   }

};

exports.postLoad = async (req, res) => {

  const Load = await Load.findById(req.params.id);

  updatedLoad = await Load.save();

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedLoad
    }
  });


};

exports.getLoadShippingInfo = async (req, res) => {
  const Load = await Load.findById(req.params.id);
  if (Load) {
   res.json({message: 'Success', Load: Load});
  }
  
  res.status(400).json({message: 'Failure'});
};
