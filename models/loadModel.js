const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({

    created_by: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },

    assigned_to: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },

    status:  {
        type: String,
        required: true
    },

    state:  {
        type: String,
        required: true
    },

    name:  {
        type: String,
        required: true
    },

    payload:  {
        type: Number,
        required: true
    },

    pickup_address:  {
        type: String,
        required: true
    },

    delivery_address:  {
        type: String,
        required: true
    },

    dimensions: {
        width:  {
            type: Number,
            required: true
        },
        length:  {
            type: Number,
            required: true
        },
        height:  {
            type: Number,
            required: true
        }
    },

    logs: [{
        message: {
            type: String,
            required: true
        },
        time: {
            type: String,
            required: true
        }
    }],

    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Load = mongoose.model('Load', loadSchema);