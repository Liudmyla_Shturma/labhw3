const mongoose = require('mongoose');

const accountSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        uniq: true
    },
    
    password: {
        type: String,
        required: true
    },

    role: {
        type: String,
        required: true
    }
});

module.exports.Account = mongoose.model('Account', accountSchema);