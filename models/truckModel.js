const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({

    created_by: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },

    assigned_to: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },

    type: {
        type: String,
        required: true
    },

    status: {
        type: String,
        required: true
    },

    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Truck = mongoose.model('Truck', truckSchema);